﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public class ChangeCursorColor : MonoBehaviour
    {
        
        public void OnMouseExit()
        {
            Cursor.SetCursor(FindObjectOfType<MenuUI>().CursorTexture[0], Vector2.down, CursorMode.Auto);
        }
        
        public void OnMouseOver()
        {
            Cursor.SetCursor(FindObjectOfType<MenuUI>().CursorTexture[1], Vector2.down, CursorMode.Auto);
        }
    }


