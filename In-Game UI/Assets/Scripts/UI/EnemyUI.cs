﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyUI : MonoBehaviour
{
    [Header("Bars")] 
    [Tooltip("the bar of the health")]
    [SerializeField] private Image _healthbar;

    [Header("Stats")]
    [Tooltip("the health of the enemy")]
    [SerializeField] private float _health = 100;

    private PlayerUI playerUI;

    // Start is called before the first frame update
    private void Start()
    {
        playerUI = FindObjectOfType<PlayerUI>();
    }
    
    public void HealthCalculation(float playerDamage)
    {
        _health -= playerDamage;
        _healthbar.fillAmount = _health / 100;

        if (!(_healthbar.fillAmount <= 0)) return;
        playerUI.ExperienceCalculation();
        Destroy(gameObject);
    }

}
