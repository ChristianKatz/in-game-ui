﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{

    [Header("Stats")]
    [Tooltip("the health of the enemy")]
    [SerializeField] private float _health = 100;
    [Tooltip("the bar of the mana")]
    [SerializeField]
    private float _mana = 100;
    private float _experience = 0;

    [Tooltip("cooldown of the first spell")]
    [SerializeField] [Range(1,10)]
    private float _firstSpellCooldown = 2;
    [Tooltip("cooldown of the second spell")]
    [SerializeField]
    [Range(1, 10)]
    private float _secondSpellCooldown = 2;
    [Tooltip("cooldown of the third spell")]
    [SerializeField]
    [Range(1, 10)]
    private float _thirdSpellCooldown = 2;

    [Header("Bars")]
    [Tooltip("the bar of the health")]
    [SerializeField] private Image _manaBar;
    [Tooltip("the bar of the health")]
    [SerializeField] private Image _healthbar;
    [Tooltip("the bar of the experience")]
    [SerializeField]
    private Image _experienceBar;

    [Header("Speel Cooldown Images")]
    [Tooltip("cooldwon image of the first spell")]
    [SerializeField] private Image _firstImage;
    [Tooltip("cooldwon image of the second spell")]
    [SerializeField] private Image _secondImage;
    [Tooltip("cooldwon image of the second spell")]
    [SerializeField] private Image _thirdImage;

    [Header("Map")]
    [Tooltip("the image for the map arrow")]
    [SerializeField] private RectTransform _mapArrow;
    [Tooltip("the minimap")] 
    [SerializeField] private Transform _minimapCamera;

    private Camera _camera;

    // Start is called before the first frame update
    private void Start()
    {
        _camera = Camera.main;
    }

    // Update is called once per frame
    private void Update()
    {

        _firstImage.fillAmount += Time.deltaTime / _firstSpellCooldown;
        _secondImage.fillAmount += Time.deltaTime / _secondSpellCooldown;
        _thirdImage.fillAmount += Time.deltaTime / _thirdSpellCooldown;

        if (_manaBar.fillAmount >= 1) return;
        _mana += Time.deltaTime * 4 ;
        _manaBar.fillAmount = _mana / 100;
    }

    private void LateUpdate()
    {
        Vector3 newPosition = transform.position;
        newPosition.y = _minimapCamera.position.y;
        _minimapCamera.position = newPosition;

        if (_camera == null) return;
        var eulerAngles = _camera.transform.eulerAngles;
        _minimapCamera.rotation = Quaternion.Euler(new Vector3(90f, _minimapCamera.rotation.y, -eulerAngles.y));
        _mapArrow.rotation = Quaternion.Euler(new Vector3(0,0,-transform.eulerAngles.y + eulerAngles.y));
    }

    public void HealthCalculation(float enemyDamage)
    {
        _health -= enemyDamage;
        _healthbar.fillAmount = _health / 100;
    }

    public void ManaCalculation(float consupmtion)
    {
        _mana -= consupmtion;
        _manaBar.fillAmount = _mana / 100;
    }

    public bool FirstSpellActivation()
    {
        if (_firstImage.fillAmount >= 1)
        {
            _firstImage.fillAmount = 0;
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool SecondSpellActivation()
    {
        if (_secondImage.fillAmount >= 1)
        {
            _secondImage.fillAmount = 0;
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool ThirdSpellActivation()
    {
        if (_thirdImage.fillAmount >= 1)
        {
            _thirdImage.fillAmount = 0;
            return true;
        }
        else
        {
            return false;
        }
    }

    public void ExperienceCalculation()
    {
        _experienceBar.fillAmount += 0.1f;

        if (_experienceBar.fillAmount >= 1)
        {
            _experienceBar.fillAmount = 0;
        }
    }
}
