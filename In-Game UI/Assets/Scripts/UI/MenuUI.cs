﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Cinemachine;

public class MenuUI : MonoBehaviour
{
    [Header("Menu Settings")]
    [Tooltip("the menu that should be displayed")]
    [SerializeField] private GameObject _menu;
    [Tooltip("the textures to change the cursor appearance")]
    [SerializeField] public Texture2D[] CursorTexture;
    [Tooltip("the HUD of the player to deactivate and activate it")]
    [SerializeField] private GameObject _hud;
    private bool _activateMainMenu;
    [Tooltip("the naimator to fade the UI")]
    [SerializeField] private RuntimeAnimatorController _fadeAnimator;

    [SerializeField] private CinemachineFreeLook _playerCamera;

    private bool _closeMenu = true;
    private bool _stopGame = false;
    public bool StopGame => _stopGame;

    [Header("Menu Structure")]
    [SerializeField]
    [Tooltip("the objects of the title")]
    private GameObject[] _title;
    [Tooltip("All Main Menu buttons")] [SerializeField]
    private GameObject[] _menuButtons;
    [Tooltip("the window of the options")][SerializeField]
    private GameObject[] _optionsWindow;
    [Tooltip(" the objects of the hourglass")]
    [SerializeField] private GameObject[] _hourglass;
    
    [Header("Settings in the Option Window")] 
    [Tooltip("the audio parent of the settings")]
    [SerializeField] private GameObject[] _audioSettings;
    [Tooltip("the graphic parent of the settings")] 
    [SerializeField] private GameObject[] _graphicSettings;
    [Tooltip("the Controls parent of the settings")]
    [SerializeField] private GameObject[] _controlsSetting;
    [Tooltip("the game parent of the settings")]
    [SerializeField] private GameObject[] _gameSettings;
    
    
    
    private void Start()
    {
        _menu.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.SetCursor(CursorTexture[0], Vector2.down, CursorMode.Auto);
    }
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && _closeMenu)
        {
            if (_activateMainMenu)
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                _activateMainMenu = false;
                _hud.SetActive(false);
                _stopGame = true;
                _playerCamera.enabled = false;
            }
            else
            {
                _menu.SetActive(false);
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                _activateMainMenu = true;
                _hud.SetActive(true);
                _stopGame = false;
                _playerCamera.enabled = true;
            }
        }

        if (_activateMainMenu) return;
        GoBack();
    }
    
    /// <summary>
    /// Fade Out the Game Objects
    /// </summary>
    /// <param name="fadeObject"></param>
    /// <returns></returns>
    public IEnumerator FadeOut(GameObject fadeObject)
    {
        if (fadeObject.GetComponent<Animator>() == null)
        {
            fadeObject.AddComponent<Animator>();
        }
        if (!fadeObject.GetComponent<Animator>().CompareTag("NoButton"))
        {
            fadeObject.GetComponent<Animator>().runtimeAnimatorController = _fadeAnimator; 
        }
        fadeObject.GetComponent<Animator>().SetTrigger("FadeOut");
        yield return new WaitForSeconds(0.15f);
        fadeObject.SetActive(false);
    }

    /// <summary>
    /// Fade In the Game Objects
    /// </summary>
    /// <param name="fadeObject"></param>
    /// <returns></returns>
    public IEnumerator FadeIn(GameObject fadeObject)
    {
        yield return new WaitForSeconds(0.15f);
        if (fadeObject.GetComponent<Animator>() == null)
        {
            fadeObject.AddComponent<Animator>();
        }
        fadeObject.SetActive(true);
        if (!fadeObject.GetComponent<Animator>().CompareTag("NoButton"))
        {
            fadeObject.GetComponent<Animator>().runtimeAnimatorController = _fadeAnimator; 
        }
        fadeObject.GetComponent<Animator>().SetTrigger("FadeIn");

    }
        /// <summary>
        /// Press Escape to go back to the Title Screen
        /// </summary>
        private void GoBack()
        {
            if(Input.GetKeyDown(KeyCode.Escape))
            {
                
                for (int i = 0; i < _optionsWindow.Length; i++)
                {
                    StartCoroutine(FadeOut(_optionsWindow[i]));
                }
                
                for (int i = 0; i < _audioSettings.Length; i++)
                {
                    StartCoroutine(FadeOut(_audioSettings[i]));
                }

                for (int i = 0; i < _graphicSettings.Length; i++)
                {
                    StartCoroutine(FadeOut(_graphicSettings[i]));
                }

                for (int i = 0; i < _gameSettings.Length; i++)
                {
                    StartCoroutine(FadeOut(_gameSettings[i]));
                }

                for (int i = 0; i < _controlsSetting.Length; i++)
                {
                    StartCoroutine(FadeOut(_controlsSetting[i]));
                }
                
                for (int i = 0; i < _menuButtons.Length; i++)
                {
                    StartCoroutine(FadeIn(_menuButtons[i]));
                }

                for (int i = 0; i < _title.Length; i++)
                {
                    StartCoroutine(FadeIn(_title[i]));
                }

                _closeMenu = true;
            }
        }

        /// <summary>
        /// close the menu
        /// </summary>
        public void Resume()
        {
            _menu.SetActive(false);
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            _activateMainMenu = true;
            _hud.SetActive(true);
            _stopGame = false;
            _playerCamera.enabled = true;
        }

        public void Save()
        {
            for (int i = 0; i < _hourglass.Length; i++)
            {
                StartCoroutine(FadeIn(_hourglass[i]));
            }
            StartCoroutine(FakeSaveGame());
        }
        
        /// <summary>
        /// exit the game
        /// </summary>
        public void Exit()
        {
            Application.Quit();
        }

        IEnumerator FakeSaveGame()
        {
            yield return new WaitForSeconds(5);
            for (int i = 0; i < _hourglass.Length; i++)
            {
                _hourglass[i].SetActive(false);
            }
            
        }
        
        /// <summary>
        /// fade in the Option Menu and fade out the title and menu Buttons
        /// </summary>
        public void OptionButton()
        {
            for (int i = 0; i < _title.Length; i++)
            {
                StartCoroutine(FadeOut(_title[i]));
            }

            for (int i = 0; i < _menuButtons.Length; i++)
            {
                StartCoroutine(FadeOut(_menuButtons[i]));
            }

            for (int i = 0; i < _optionsWindow.Length; i++)
            {
                StartCoroutine(FadeIn(_optionsWindow[i]));
            }

            for (int i = 0; i < _audioSettings.Length; i++)
            {
                StartCoroutine(FadeIn(_audioSettings[i]));
            }

            _closeMenu = false;
        }
        public void GraphicSettings()
        {
            for (int i = 0; i < _controlsSetting.Length; i++)
            {
                StartCoroutine(FadeOut(_controlsSetting[i]));
            }

            for (int i = 0; i < _gameSettings.Length; i++)
            {
                StartCoroutine(FadeOut(_gameSettings[i]));
            }

            for (int i = 0; i < _audioSettings.Length; i++)
            {
                StartCoroutine(FadeOut(_audioSettings[i]));
            }

            for (int i = 0; i < _graphicSettings.Length; i++)
            {
                StartCoroutine(FadeIn(_graphicSettings[i]));
            }
        }

        public void AudioSettings()
        {
            for (int i = 0; i < _controlsSetting.Length; i++)
            {
                StartCoroutine(FadeOut(_controlsSetting[i]));
            }

            for (int i = 0; i < _gameSettings.Length; i++)
            {
                StartCoroutine(FadeOut(_gameSettings[i]));
            }

            for (int i = 0; i < _graphicSettings.Length; i++)
            {
                StartCoroutine(FadeOut(_graphicSettings[i]));
            }

            for (int i = 0; i < _audioSettings.Length; i++)
            {
                StartCoroutine(FadeIn(_audioSettings[i]));
            }
        }

        public void GameSettings()
        {
            for (int i = 0; i < _controlsSetting.Length; i++)
            {
                StartCoroutine(FadeOut(_controlsSetting[i]));
            }

            for (int i = 0; i < _audioSettings.Length; i++)
            {
                StartCoroutine(FadeOut(_audioSettings[i]));
            }

            for (int i = 0; i < _graphicSettings.Length; i++)
            {
                StartCoroutine(FadeOut(_graphicSettings[i]));
            }

            for (int i = 0; i < _gameSettings.Length; i++)
            {
                StartCoroutine(FadeIn(_gameSettings[i]));
            }
        }

        public void ControlsSettings()
        {
            for (int i = 0; i < _gameSettings.Length; i++)
            {
                StartCoroutine(FadeOut(_gameSettings[i]));
            }

            for (int i = 0; i < _audioSettings.Length; i++)
            {
                StartCoroutine(FadeOut(_audioSettings[i]));
            }

            for (int i = 0; i < _graphicSettings.Length; i++)
            {
                StartCoroutine(FadeOut(_graphicSettings[i]));
            }

            for (int i = 0; i < _controlsSetting.Length; i++)
            {
                StartCoroutine(FadeIn(_controlsSetting[i]));
            }
        }

}
