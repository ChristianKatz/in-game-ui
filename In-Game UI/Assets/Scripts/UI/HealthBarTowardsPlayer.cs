﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarTowardsPlayer : MonoBehaviour
{
    private Transform _enemyHealthBar;
    
    private void Start()
    {
        _enemyHealthBar = GetComponent<Transform>();
    }

    private void Update()
    {
        var direction = Camera.main.transform.position  - transform.position;
        _enemyHealthBar.rotation = Quaternion.LookRotation(new Vector3(direction.x,0,direction.z));
    }
}
