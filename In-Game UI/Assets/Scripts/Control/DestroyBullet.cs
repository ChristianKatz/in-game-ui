﻿using System;
using UnityEngine;

namespace UEGP3CA.Controller
{
    public class DestroyBullet : MonoBehaviour
    {
        [Header("Bullet Settings")]
        [Tooltip("the radius of the trajectory point check")]
        [SerializeField] private float _radiusCheck;
        [Tooltip("the Layer Mask for the trajectory points")]
        [SerializeField] private LayerMask _trajectoryMask;
        [Tooltip("the tag of the last trajectoryPoint")]
        [SerializeField] private string _lastPointTag = "lastPoint";

        // says if the last point of the trajectory is reached
        public bool ReachedLastPoints { get; private set; } = false;

        private void Update()
        {
            // get the trajectory points, which the bullet is passing
            var trajectoryPoints = Physics.OverlapSphere(transform.position, _radiusCheck, _trajectoryMask);

            // check if the last point is reached
            foreach (var points in trajectoryPoints)
            {
                if (points.CompareTag(_lastPointTag))
                {
                    ReachedLastPoints = true;
                }
            }
        }

        /// <summary>
        /// destroy the bullet, when it hits the ground or enemy
        /// </summary>
        /// <param name="other"></param>
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Land") || other.CompareTag("Enemy"))
            {
                ReachedLastPoints = true;
            }
        }
    }
}
