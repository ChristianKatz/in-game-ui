﻿using System.Collections;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

namespace UEGP3CA.Controller
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerController : MonoBehaviour
    {
        [Header("Move Settings")]
        [Tooltip("set the speed of the character")]
        [SerializeField] private float _movementSpeed;
        [Tooltip("smooth the character rotation")]
        [SerializeField] private float _smoothRotation;
        [Tooltip("smooth the movement of the character")]
        [SerializeField] private float _smoothSpeed;
        [Tooltip("multiplies the gravity of the player")]
        [SerializeField] private float _gravityModifier;
        // the ref Velocity of the WASD Movement
        private float _refVelocity;
        // the current Velocity of the WASD Movement
        private float _currentforwardVelocity;
        // the _direction where the player wants to move
        private Vector3 _direction;
        // velocity of the character
        private Vector3 _velocity;
        private PlayerUI _playerUI;

        [Header("Jump Settings")]
        [Tooltip("the position where the character ground check should be placed")]
        [SerializeField] private Transform _groundCheckPosition;
        [Tooltip("the ground check radius around the position of the check")] 
        [SerializeField] private float _groundCheckRadius;
        [Tooltip("the layer mask where the ground will be registered")] 
        [SerializeField] private LayerMask _groundLayerMask;
        [Tooltip("determines the jump height of the character")]
        [SerializeField] private float _jumpHeight;
        // the velocity during the jump
        private float _jumpVelocity; 
        // the velocity when the player is falling
        private float _downVelocity;
        // check if the player is grounded
        private bool _isGrounded = true;
        // activate the double jump
        private bool _activateDoubleJump = false;

        [Tooltip("the time that the player is able to jump when not grounded")]
        [SerializeField] private float _setCoyoteTime = 0.2f;
        // the time where the player has still a change to jump when not grounded
        private float _coyoteTime = 0;
        // be sure to not use the jump, when player is not grounded
        private bool _useCoyoteTime = false;

        [Header("Auto Aim")]
        [Tooltip("the tag for the enemy that should be the aimed")]
        [SerializeField] private string _enemyTag = "Enemy";
        [Tooltip("the layer for the enemy that should be aimed")]
        [SerializeField] private string _enemyLayer = "Enemy";
        [Tooltip("the radius where the auto aim can be used")]
        [SerializeField] private float _autoAimRadius = 5;
        [Tooltip("how fast the character should rotate to the enemy")]
        [SerializeField] private float _smoothRotateToEnemy = 0.5f;
        [Tooltip("the time which the character aims automatically")]
        [SerializeField] private float _aimDuration = 2;
        [Tooltip("the game object for the enemy mark")]
        [SerializeField] GameObject _targetMark;
        [Tooltip("offset for the enemy mark")] 
        [SerializeField] private Vector3 _targetMarkOffset = new Vector3(0,2,0);
        // if the closest enemy is found, the player rotates to the enemy
        private bool _rotateToEnemy = false;
        // the direction for the aim
        private Vector3 _aimDirection;
        // the enemy, that will be found
        private Transform _foundEnemy = null;
        // the instantiated target mark
        GameObject _instantiatedTargetMark;
        // show if the aim is activated or not
        private bool _autoAimActivated = false;
        // take the initial auto aim duration
        private float _initialAimDuration;

        [Header("Mortar")] 
        [Tooltip("the position, where the bullet is spawning")] 
        [SerializeField] private Transform _shootPosition;
        [Tooltip("the prefab of the bullet")] 
        [SerializeField] private GameObject _bullet;
        [Tooltip("the max force that the mortar can have")]
        [SerializeField] private float _maxShootPower;
        [Tooltip("the charge multiplier how fast it should charge to the max power value")] 
        [SerializeField] private float _chargeMultiplier = 20;
        [Tooltip("the seconds, where the bullets gets destroyed, after the bullet reached the last trajectory point")]
        [SerializeField] private float _secondsToDestroyBullet;
        // the shoot power that was charged
        private float _currentShootPower = 0;
        // the bullet that was instantiated at the mortar 
        private GameObject _instantiatedBullet;
        // deactivate or activate the methods that are needed
        private bool _useMortar = false;
        // the script that destroys the bullet
        private DestroyBullet _destroyBullet;
        //prevent that the next shot can be fired immediately
        private bool _bulletFired = false;
        
        [Header("Trajectory for the Mortar")]
        [Tooltip("the prefab of the points to shows the trajectory")]
        [SerializeField] private GameObject _pointsPrefab; 
        [Tooltip("the number of the points")]
        [SerializeField] private int _numberOfPoints;
        // all trajectory points
       private GameObject[] _trajectoryPoints;
       
        // the vertical input
        private float _verticalAxis;
        // the horizontal input
        private float _horizontalAxis;
        // the controller of the character
        private CharacterController _characterController;
        // the main camera
        private Camera _camera;

        private MenuUI _menuUi;

        private void Start()
        {
            _menuUi = FindObjectOfType<MenuUI>();
            
            // get the main camera
            _camera = Camera.main;
            
            // the number of the bullets will be declared as Game Object
            _trajectoryPoints = new GameObject[_numberOfPoints];

            // spawn all objects of the trajectory points and deactivate them, because we need them only when the player wants to use the Mortar
            for (var i = 0; i < _numberOfPoints; i++)
            {
                _trajectoryPoints[i] = Instantiate(_pointsPrefab, transform.position, Quaternion.identity);
                _trajectoryPoints[i].SetActive(false);
            }
            
            // set the aim duration to give the initial value back when the player stops the auto aim
            _initialAimDuration = _aimDuration;

            // the jump velocity that the player has when he jumps
            _jumpVelocity = Mathf.Sqrt(_jumpHeight * -2 * Physics.gravity.y);

            // get the character controller
            _characterController = GetComponent<CharacterController>();

            _playerUI = GetComponent<PlayerUI>();
        }

        private void Update()
        {
            if (_menuUi.StopGame) return;
            RotateToEnemy();
            MeleeAttack();
            SpellInput();

            // if the player presses the button, the player will aim at the closest enemy
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                // activate the auto aim, when it isn't already
                if (!_autoAimActivated && !_useMortar)
                { 
                    AutoAim();
                    _autoAimActivated = true;
                }
                // deactivate the auto aim, when the player wants to
                else
                {
                    // the player interrupts the aim so that the aim duration is 0
                    _aimDuration = 0;
                    // stop the already started coroutines to don't come in any conflict
                    StopAllCoroutines();
                    // start the coroutine to stop the aim
                    StartCoroutine(AimTime());
                    _autoAimActivated = false;
                }
            }

            // update the gap between each point
            for (int i = 0; i < _trajectoryPoints.Length; i++)
            {
                _trajectoryPoints[i].transform.position = PointPosition(i * 0.1f);
            }

            // get the input of the direction
            _verticalAxis = Input.GetAxisRaw("Vertical");
            _horizontalAxis = Input.GetAxisRaw("Horizontal");

            // the direction where the character is going to move
            _direction = (_camera.transform.right * _horizontalAxis + _camera.transform.forward * _verticalAxis).normalized;

            Mortar();
            // if the player uses the mortar all movement will be canceled
            if (_useMortar) return;
            Rotation();
            Jump();
            Move();
        }
        
        private void MeleeAttack()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Collider[] enemyColliders = Physics.OverlapSphere(transform.position, 5, LayerMask.GetMask("Enemy"));

                foreach (Collider enemy in enemyColliders)
                {
                    enemy.GetComponent<EnemyUI>().HealthCalculation(10);
                }
            }
        }
        
        private void SpellInput()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1) && _playerUI.FirstSpellActivation())
            {
                _playerUI.ManaCalculation(10);
            }

            if (Input.GetKeyDown(KeyCode.Alpha2) && _playerUI.SecondSpellActivation())
            {
                _playerUI.ManaCalculation(10);
            }

            if (Input.GetKeyDown(KeyCode.Alpha3) && _playerUI.ThirdSpellActivation())
            {
                _playerUI.ManaCalculation(10);
            }
        }
        
        /// <summary>
        /// handles the Mortar
        /// </summary>
        private void Mortar()
        {
           // When the left Mouse Button is clicked and a bullet was not already fired the mortar shot starts to charge
            if (Input.GetMouseButton(1) && !_bulletFired && !_autoAimActivated && _isGrounded)
            {
                // charge the power of the shot
                _currentShootPower += Time.deltaTime * _chargeMultiplier;
                // clamp the maximal power of the shot
                _currentShootPower = Mathf.Clamp(_currentShootPower, 0, _maxShootPower);
                // deactivate the rest of the movement what ist not needed
                _useMortar = true;

                // when the player uses the mortar all points get the trajectoryPoints Layer, thus the bullet script can check the points
                // all points will be activated
                for (int i = 0; i < _numberOfPoints; i++)
                {
                    _trajectoryPoints[i].layer = 11;
                    _trajectoryPoints[i].SetActive(true);
                }
                
                // the last trajectory point to activate the destruction of the bullet to prepare the reactivation of the Mortar
                _trajectoryPoints[_trajectoryPoints.Length -1].tag = "lastPoint";
            }

            // when the left Mouse Button is up and a bullet is not already fired, the bullet will be spawned and will be shot in the air
            else if (Input.GetMouseButtonUp(1) && !_bulletFired && !_autoAimActivated && _isGrounded)
            {   
                // blocks that the player can shoot more bullets than one at the same time
                _bulletFired = true;
                _instantiatedBullet = Instantiate(_bullet, _shootPosition.position, Quaternion.identity);
                _instantiatedBullet.GetComponent<Rigidbody>().AddForce((transform.forward + transform.up) * _currentShootPower, ForceMode.Impulse);
                // get the script of the bullet
                _destroyBullet = FindObjectOfType<DestroyBullet>();
            }

            // Start the destroy cooldown when the last trajectory point is reached, that is the reason why I need the bullet script
            if (_destroyBullet != null && _destroyBullet.ReachedLastPoints)
            {
                StartCoroutine(DestroyShot());
            }
        }

        /// <summary>
        /// every point position for the trajectory
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        private Vector3 PointPosition(float t)
        {
            // the formula to show the trajectory of the projectile
            var currentPointPos = _shootPosition.position + ((transform.forward + transform.up) * (_currentShootPower * t)) + Physics.gravity * (0.5f * (t * t));
            return currentPointPos;
        }

        /// <summary>
        /// Destroy the bullet and make the Mortar reusable
        /// </summary>
        /// <returns></returns>
        private IEnumerator DestroyShot()
        {
            // after a fixed time, the following code will be executed
            yield return new WaitForSeconds(_secondsToDestroyBullet);
            // destroy the instantiated bullet
            Destroy(_instantiatedBullet);
            // set the old value back to charge the shot from 0 again
            _currentShootPower = 0;
            // the player is able to move "normal" again
            _useMortar = false;
            // deactivates the trajectory points for the next charge
            for (var i = 0; i < _numberOfPoints; i++)
            {
                _trajectoryPoints[i].SetActive(false);
            }
            // allows the player to shoot again
            _bulletFired = false;
        }

        /// <summary>
        /// character rotates to the closest enemy
        /// </summary>
        private void AutoAim()
        {
            // all enemies that could be found in a fixed radius
            var enemies = Physics.OverlapSphere(transform.position, _autoAimRadius, LayerMask.GetMask(_enemyTag));

            // take the distance of the enemy to search if there is someone closer
            var currentClosestEnemy = Mathf.Infinity;
            foreach (var enemy in enemies)
            {
                // the distance of the found enemy
                var newEnemyDistance = Vector3.Distance(transform.position, enemy.transform.position);

                // compare if the new enemy is closer
                if (!(currentClosestEnemy >= newEnemyDistance)) continue;
                // take the closest enemy
                _foundEnemy = enemy.transform;
                // set the closest enemy distance
                currentClosestEnemy = newEnemyDistance;
            }

            // if the closest enemy was found the aiming can begin
            if (_foundEnemy != null && _foundEnemy.CompareTag(_enemyTag))
            {
                // spawn the target mark over the head of the enemy
                _instantiatedTargetMark = Instantiate(_targetMark, _foundEnemy.transform.position + _targetMarkOffset, Quaternion.identity);
                // the player begins to rotate to the enemy
                _rotateToEnemy = true;
                // the time will run to deactivate the auto aim
                StartCoroutine(AimTime());
            }
        }

        /// <summary>
        /// rotates the character to the enemy, if the auto aim was activated
        /// </summary>
        private void RotateToEnemy()
        {
            if (_rotateToEnemy && _foundEnemy != null)
            {
                // rotates the character to the enemy direction
                _aimDirection = _foundEnemy.transform.position - transform.position;
                var rotateToEnemy = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(new Vector3(_aimDirection.x, 
                    0, _aimDirection.z), Vector3.up), _smoothRotateToEnemy * Time.deltaTime);
                transform.rotation = rotateToEnemy;
                
                // the raycast that set the laser position
                Physics.Raycast(transform.position, transform.forward * 100, out var hit, LayerMask.GetMask(_enemyLayer));
                // update the position of the target mark
                _instantiatedTargetMark.transform.position = _foundEnemy.transform.position + _targetMarkOffset;
                
                // the direction of the rotation
                Vector3 _diplayDirection = Camera.main.transform.position - transform.position;
                // rotates the target mark to the enemy
                _instantiatedTargetMark.transform.rotation = Quaternion.LookRotation(new Vector3(_diplayDirection.x, 0, _diplayDirection.z));
                
                // calculate the distance between the character and the enemy
                float distance = Vector3.Distance(transform.position, _foundEnemy.transform.position);
                // if the distance is bigger than the auto aim distance, the aiming will stop
                if (distance > _autoAimRadius && _autoAimActivated)
                {
                    // it interrupts the aiming so that the aim duration is 0
                    _aimDuration = 0;
                    // stop the already started coroutines to don't come in any conflict
                    StopAllCoroutines();
                    // start the coroutine to stop the aim
                    StartCoroutine(AimTime());
                    _autoAimActivated = false;
                }
            }

            else if (_foundEnemy == null && _rotateToEnemy && _autoAimActivated)
            {
                // it interrupts the aiming so that the aim duration is 0
                _aimDuration = 0;
                // stop the already started coroutines to don't come in any conflict
                StopAllCoroutines();
                // start the coroutine to stop the aim
                StartCoroutine(AimTime());
                _autoAimActivated = false;
            }
        }

        private IEnumerator AimTime()
        {
            // set the time duration
            yield return new WaitForSeconds(_aimDuration);
            // the rotation to the enemy will stop
            _rotateToEnemy = false;
            // target mark will be destroyed
            Destroy(_instantiatedTargetMark);
            // there is no one left to aim, so set to null
            _foundEnemy = null;
            // deactivate the auto aim, so that it can reused
            _autoAimActivated = false;
            // set the initial aim duration for the next aim if the player doesn't interrupt the aiming
            _aimDuration = _initialAimDuration;
        }

        /// <summary>
        /// the gravity of the character
        /// </summary>
        /// <returns></returns>
        private float Gravity()
        {
            return _downVelocity += Physics.gravity.y * _gravityModifier * Time.deltaTime;
        }

        /// <summary>
        /// Rotates the character in the desired direction on the X and Z Axis
        /// </summary>
        private void Rotation()
        {
            if (_direction != Vector3.zero && !_autoAimActivated)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(new Vector3(_direction.x, 0, _direction.z)), _smoothRotation * Time.deltaTime);
            }
        }

        /// <summary>
        /// Move the character in the desired _direction on the X and Z Axis
        /// </summary>
        private void Move()
        {
            // the current velocity will be smoothed, so that we have some tweaks 
            _currentforwardVelocity = Mathf.SmoothDamp(_currentforwardVelocity, _direction.magnitude * _movementSpeed, ref _refVelocity, _smoothSpeed * Time.deltaTime);

            // the velocity of the character
            _velocity = transform.forward * _currentforwardVelocity + new Vector3(0, Gravity(), 0);

            // if the auto aim is activated, the other movement will be set, because the rotation is always in the direction of the enemy
            // and the old movement can only move the character in the Z direction, that makes left and right movement impossible
            if (_autoAimActivated)
            {
                _velocity = new Vector3(_direction.x * _currentforwardVelocity, 0, _direction.z * _currentforwardVelocity) + new Vector3(0, Gravity(), 0);
            }

            // the velocity will put in to move the character
            _characterController.Move(_velocity * Time.deltaTime);
        }

        /// <summary>
        /// check if the character is grounded to jump or to add down velocity
        /// </summary>
        private void Jump()
        {
            // check if the character is grounded
             _isGrounded = Physics.CheckSphere(_groundCheckPosition.position, _groundCheckRadius, _groundLayerMask);

            // if the character is not grounded, the downVelocity will be increased in the Method "Gravity"
            if (_isGrounded)
            {
                // reset the _useCoyoteTime to use it again when the character is not grounded
                _useCoyoteTime = true;
                // reset the time for the next chance to jump when not grounded
                _coyoteTime = _setCoyoteTime;

                // the downVelocity is 0, when the player is grounded
                _downVelocity = 0;

                // press the button to jump
                if (!Input.GetKeyDown(KeyCode.Space)) return;
                _downVelocity = _jumpVelocity;
                // Coyote Time should only be used when the player hasn't jump off the ground
                _useCoyoteTime = false;
                // activate the double jump
                _activateDoubleJump = true;
            }
            else
            {
                // if the character is not grounded, the player has still time to jump
                _coyoteTime -= Time.deltaTime;

                // if the time is over, the player can't use the jump anymore
                if (_coyoteTime >= 0)
                {
                    // press the button to jump
                    if (Input.GetKeyDown(KeyCode.Space) && _useCoyoteTime)
                    {
                        _downVelocity = _jumpVelocity;
                        // reset the coyote time
                        _coyoteTime = _setCoyoteTime;
                        // the mechanic was used
                        _useCoyoteTime = false;
                        // activate the double jump
                        _activateDoubleJump = true;
                        return;
                    }
                }
                // if the player is not grounded and jumped already, he gets a second jump
                if (!Input.GetKeyDown(KeyCode.Space) || !_activateDoubleJump) return;
                _downVelocity = _jumpVelocity;
                _activateDoubleJump = false;

            }
        }
        
        /// <summary>
        /// Draw Gizmos to simplify the modification
        /// </summary>
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(_groundCheckPosition.position, _groundCheckRadius);
            Gizmos.DrawWireSphere(transform.position, _autoAimRadius);
        }
    }
}

